resource "aws_security_group" "allow_pub_instance_traffic" {
  name        = "allow_pub_instance_traffic"
  description = "allow traffic from public instance"

  ingress {
    from_port = 0
    to_port =  0
    protocol    = "-1"
    cidr_blocks = ["10.0.1.0/24"]
  }
  vpc_id="${aws_vpc.default.id}"
}

resource "aws_security_group" "allow_ssh" {
  name        = "allow-ssh"
  description = "Allow SSH inbound traffic"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id="${aws_vpc.default.id}"

}

resource "aws_security_group" "allow_nginx" {
  name        = "allow_nginx"
  description = "allow nginx service"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id="${aws_vpc.default.id}"

}

resource "aws_security_group" "allow_outbound" {
  name        = "allow-all-outbound"
  description = "Allow all outbound traffic"

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  vpc_id="${aws_vpc.default.id}"
}