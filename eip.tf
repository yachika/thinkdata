resource "aws_eip" "for-nat" {
    vpc = true
}

resource "aws_eip" "test-eip" {
  instance    = "${aws_instance.public-instance.id}"
}
