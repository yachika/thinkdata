resource "tls_private_key" "pub-key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "generated_key" {
  key_name   = "${var.key_name}"
  public_key = "${tls_private_key.pub-key.public_key_openssh}"
}

resource "aws_instance" "public-instance" {
    ami             = "${data.aws_ami.ubuntu.id}"
    instance_type   = "t2.micro"
    key_name        = "${aws_key_pair.login-key.key_name}"
    subnet_id = "${aws_subnet.public-subnet.id}"
    associate_public_ip_address = true
    source_dest_check = false
    vpc_security_group_ids = ["${aws_security_group.allow_ssh.id}",
                                "${aws_security_group.allow_outbound.id}",
                                "${aws_security_group.allow_nginx.id}"]
    tags {
        Name = "public-instance"
    }
}

resource "aws_instance" "private-instance" {
    ami = "${data.aws_ami.ubuntu.id}"
    instance_type = "t2.micro"
    key_name = "${aws_key_pair.login-key.key_name}"
    vpc_security_group_ids = ["${aws_security_group.allow_pub_instance_traffic.id}",
                                "${aws_security_group.allow_outbound.id}"]
    subnet_id = "${aws_subnet.private-subnet.id}"
    associate_public_ip_address = false
    source_dest_check = false
    tags {
        Name = "private instance"
    }
}

