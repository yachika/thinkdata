resource "aws_key_pair" "login-key" {
  key_name   = "login-key"
  public_key = "${file("login-key.pub")}"
}