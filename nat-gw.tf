resource "aws_nat_gateway" "gw" {
  allocation_id = "${aws_eip.for-nat.id}"
  subnet_id     = "${aws_subnet.public-subnet.id}"
  depends_on = ["aws_internet_gateway.gw"]
  tags = {
    Name = "gw NAT"
  }
}